//
//  Rasterizer.cpp
//
//  Created by Warren R. Carithers on 01/28/14.
//  Based on a C++ version created by Joe Geigel on 11/30/11.
//  Copyright 2014 Rochester Institute of Technology. All rights reserved.
//
//  Contributor:  YOUR_NAME_HERE
//

#include <cmath>
#include <iostream>
#include <sstream>

#include "Rasterizer.h"

///
//
// Simple class that performs rasterization algorithms
//
///

///
// Constructor
//
// @param n number of scanlines
// @param C the Canvas to be used
///

Rasterizer::Rasterizer( int n, Canvas &canvas ) : n_scanlines(n), C(canvas)
{
}

///
// Draw my initials
//
// Draw my own initials using calls to drawLine(), in the same
// manner that makeLines() in the driver program draws 'CG'.
///

void drawH(Rasterizer &R, int startX) {
    auto startOfMidBar = startX + 60;
    auto endOfMidBar = startOfMidBar + 90;
    auto endOfRightSide = endOfMidBar + 60;
    auto top = 300;
    auto bottom = 50;
    auto midBarTop = 145;
    auto midBarBottom = 205;

    R.drawLine(startX, bottom, startX, top);
    R.drawLine(endOfRightSide, bottom, endOfRightSide, top);

    R.drawLine(startX, bottom, startOfMidBar, bottom);
    R.drawLine(startX, top, startOfMidBar, top);
    
    R.drawLine(endOfMidBar, bottom, endOfRightSide, bottom);
    R.drawLine(endOfMidBar, top, endOfRightSide, top);

    R.drawLine(startOfMidBar, bottom, startOfMidBar, midBarTop);
    R.drawLine(startOfMidBar, midBarBottom, startOfMidBar, top);

    R.drawLine(startOfMidBar, midBarTop, endOfMidBar, midBarTop);
    R.drawLine(startOfMidBar, midBarBottom, endOfMidBar, midBarBottom);

    R.drawLine(endOfMidBar, midBarBottom, endOfMidBar, top);
    R.drawLine(endOfMidBar, bottom, endOfMidBar, midBarTop);

}

void Rasterizer::myInitials( void ) {

    // ######## Use light blue (0.678,0.847,0.902) for initials ######## 

    C.setColor( 0.678, 0.847, 0.902 );

    //
    // add code here to draw your initials
    // with calls to your drawLine() function
    //

    drawH(*this, /* startX = */40);
    drawH(*this, /* startX = */350);
}

//
// Draw a line from (x0,y0) to (x1, y1)
//
// Implementation should be using the Midpoint Line Algorithm.
//
// You are to add the implementation here using only calls to the
// setPixel() method of the Canvas.
//
// @param x0 x coord of first endpoint
// @param y0 y coord of first endpoint
// @param x1 x coord of second endpoint
// @param y1 y coord of second endpoint
//
void Rasterizer::drawLine( int x0, int y0, int x1, int y1 )
{
    // If x0 is to the right of x1, swap them for the line.
    if (x0 > x1) {
        std::swap(x0, x1);
        std::swap(y0, y1);
    }

    int dx = x1 - x0;
    int dy = y1 - y0;

    // If we're traveling in a negative slope, then step y by -1 each iteration.
    // The goal here is to normalize the main algorithm to the "small positive
    // slope" case, and re-transpose and swap pixels right when we set them.
    int yStep;
    if (dy < 0) {
        yStep = -1;
        dy *= -1;
    } else {
        yStep = 1;
    }

    bool isSmallSlope = dy < dx;

    // If we're dealing with a small slope, then reflect over the XY axis.
    if (!isSmallSlope) {
        std::swap(dx, dy);
        std::swap(x0, y0);
        std::swap(x1, y1);

        // Swap them again if we've transposed this pair of coordinates.
        if (x0 > x1) {
            std::swap(x0, x1);
            std::swap(y0, y1);
        }
    }

    auto decision = (2 * dy) - dx;
    auto y = y0;
    auto dNE = 2 * (dy - dx);
    auto dE = 2 * dy;
    for (int x = x0; x <= x1; ++x) {
        // Re-transpose back to original coordinates if it's not
        // a small slope.
        if (isSmallSlope) {
            C.setPixel(x, y);
        } else {
            C.setPixel(y, x);
        }

        if (decision < 0) {
            decision += dE;
        } else {
            decision += dNE;
            y += yStep;
        }
    }
}
