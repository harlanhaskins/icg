//
//  Textures
//
//  Created by Warren R. Carithers 2016/11/22.
//  Based on code created by Joe Geigel on 1/23/13.
//  Copyright 2016 Rochester Institute of Technology.  All rights reserved.
//
//  Contributor:  Harlan Haskins
//
//  Simple class for setting up texture mapping parameters.
//
//  This code can be compiled as either C or C++.
//

#ifdef __cplusplus
#include <iostream>
#include <map>
#else
#include <stdio.h>
#endif

#include "Textures.h"

// this is here in case you are using SOIL;
// if you're not, it can be deleted.
#include <SOIL/SOIL.h>

#ifdef __cplusplus
using namespace std;
#endif

// Add any global definitions and/or variables you need here.
GLuint smileTexture = 0;
GLuint frownTexture = 0;

GLuint loadTexture(const char *filename) {
  auto tex = SOIL_load_OGL_texture(
    filename,
    SOIL_LOAD_AUTO,
    SOIL_CREATE_NEW_ID,
    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_TEXTURE_REPEATS
  );

  if (tex == 0) {
    cerr << "Failed loading texture '" << filename << "': "
         << SOIL_last_result() << endl;
    exit(-1);
  }

  return tex;
}

///
// This function loads texture data for the GPU.
//
// You will need to write this function, and maintain all of the values
// needed to be sent to the various shaders.
//
void loadTexture(void) {
  smileTexture = loadTexture("smiley2.png");
  frownTexture = loadTexture("frowny2.png");
}

void setTexture(GLuint program, const char *name, unsigned textureUnit, GLuint handle) {
  auto loc = glGetUniformLocation(program, name);
  glActiveTexture(GL_TEXTURE0 + textureUnit);
  glBindTexture(GL_TEXTURE_2D, handle);
  glUniform1i(loc, textureUnit);
}

///
// This function sets up the parameters for texture use.
//
// You will need to write this function, and maintain all of the values
// needed to be sent to the various shaders.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
///
void setUpTexture(GLuint program) {
  setTexture(program, "smileTexture", 0, smileTexture);
  setTexture(program, "frownTexture", 1, frownTexture);
}
