//
//  Lighting
//
//  Created by Warren R. Carithers 2016/11/22.
//  Based on code created by Joe Geigel on 1/23/13.
//  Copyright 2016 Rochester Institute of Technology.  All rights reserved.
//
//  Contributor: Harlan Haskins
//
//  Simple class for setting up Phong illumination/shading.
//
//  This code can be compiled as either C or C++.
//

#include "Lighting.h"

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

void setUniform(GLuint program, const char *name, glm::vec4 input) {
  auto loc = glGetUniformLocation(program, name);
  glUniform4fv(loc, 1, glm::value_ptr(input));
}

void setUniform(GLuint program, const char *name, float input) {
  auto loc = glGetUniformLocation(program, name);
  glUniform1f(loc, input);
}

///
// This function sets up the lighting, material, and shading parameters
// for the Phong shader.
//
// You will need to write this function, and maintain all of the values
// needed to be sent to the vertex shader.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
///
void setUpPhong(GLuint program) {
  setUniform(program, "lightPos", glm::vec4(0.0, 5.0, 2.0, 1.0));
  setUniform(program, "ambientColor", glm::vec4(0.5, 0.1, 0.9, 1.0));
  setUniform(program, "diffuseColor", glm::vec4(0.89, 0.0, 0.0, 1.0));
  setUniform(program, "specularColor", glm::vec4(1.0, 1.0, 1.0, 1.0));
  setUniform(program, "lightColor", glm::vec4(1.0, 1.0, 0.0, 1.0));
  setUniform(program, "ambientLightColor", glm::vec4(0.5, 0.5, 0.5, 1.0));
  setUniform(program, "ambientCoeff", 0.5);
  setUniform(program, "diffuseCoeff", 0.7);
  setUniform(program, "specCoeff", 1.0);
  setUniform(program, "specExp", 10.0);
}
