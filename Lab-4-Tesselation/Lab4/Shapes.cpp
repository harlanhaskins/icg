//
//  Shapes
//
//  Routines for tessellating a number of basic shapes
//
//  Students are to supply their implementations for the functions in
//  this file using the function addTriangle() to do the tessellation.
//
//  This code can be compiled as either C or C++.
//

#include <cmath>
#include <glm/glm.hpp>
#include <iostream>

// Canvas.h pulls in all the OpenGL/GLFW/etc. header files for us
#include "Canvas.h"
#include "Shapes.h"
#include "Vertex.h"


/**
 Breaks a quad (specified in counter-clockwise order) into triangles, and
 adds them both to the canvas in counter-clockwise order.

 @param C The canvas to draw into.
 @param lr The lower right vertex of the quad.
 @param ur The upper right vertex of the quad.
 @param ul The upper left vertex of the quad.
 @param ll The lower left vertex of the quad.
 */
static void addQuad(Canvas &C, Vertex lr, Vertex ur, Vertex ul, Vertex ll) {
  C.addTriangle(lr, ur, ul);
  C.addTriangle(lr, ul, ll);
}

/**
 Gets a vector representing the difference between two vertices.

 @param start The starting vertex.
 @param end The ending vertex.
 @return A vector with the same direction and magnitude as the line segment from
         start to end, centered at the origin.
 */
static glm::vec3 differenceVector(Vertex start, Vertex end) {
  return glm::vec3(end.x, end.y, end.z) - glm::vec3(start.x, start.y, start.z);
}


/**
 Subdivides and adds a quad (in counter-clockwise order) to the provided canvas.

 @param C The canvas to draw into.
 @param subdivisions The number of subdivisions to subdivide the square
                     vertically.
 @param lr The lower right vertex of the quad.
 @param ur The upper right vertex of the quad.
 @param ul The upper left vertex of the quad.
 @param ll The lower left vertex of the quad.
 */
static void addQuad(Canvas &C, int subdivisions, Vertex lr,
                    Vertex ur, Vertex ul, Vertex ll) {
  float length = 1.0f / subdivisions;
  glm::vec3 horiz = differenceVector(lr, ll) * length;
  glm::vec3 vert = differenceVector(ll, ul) * length;
  for (float v = 0; v < subdivisions; ++v) {
    for (float h = 0; h < subdivisions; ++h) {
      addQuad(C,
              lr + (horiz * h)        + (vert * v),
              lr + (horiz * h)        + (vert * (v + 1)),
              lr + (horiz * (h + 1))  + (vert * (v + 1)),
              lr + (horiz * (h + 1))  + (vert * v));
    }
  }
}

///
// makeCube - Create a unit cube, centered at the origin, with a given number
// of subdivisions in each direction on each face.
//
// @param subdivision - number of equal subdivisions to be made in each
//        direction along each face
//
// Can only use calls to C.addTriangle()
///
void makeCube(Canvas &C, int subdivisions) {
  if (subdivisions < 1)
    subdivisions = 1;

  // front face
  addQuad(C, subdivisions,
          { 0.5, -0.5, -0.5},
          { 0.5,  0.5, -0.5},
          {-0.5,  0.5, -0.5},
          {-0.5, -0.5, -0.5});

  // back face
  addQuad(C, subdivisions,
          {-0.5, -0.5, 0.5},
          {-0.5,  0.5, 0.5},
          { 0.5,  0.5, 0.5},
          { 0.5, -0.5, 0.5});

  // left face
  addQuad(C, subdivisions,
          {-0.5, -0.5, -0.5},
          {-0.5,  0.5, -0.5},
          {-0.5,  0.5,  0.5},
          {-0.5, -0.5,  0.5});

  // right face
  addQuad(C, subdivisions,
          {0.5, -0.5,  0.5},
          {0.5,  0.5,  0.5},
          {0.5,  0.5, -0.5},
          {0.5, -0.5, -0.5});

  // top face
  addQuad(C, subdivisions,
          { 0.5, 0.5, -0.5},
          { 0.5, 0.5,  0.5},
          {-0.5, 0.5,  0.5},
          {-0.5, 0.5, -0.5});

  // bottom face
  addQuad(C, subdivisions,
          { 0.5, -0.5,  0.5},
          { 0.5, -0.5, -0.5},
          {-0.5, -0.5, -0.5},
          {-0.5, -0.5,  0.5});
}

static Vertex alongCircle(float y, float radius, float rads) {
  return { sin(rads) * radius, y, cos(rads) * radius };
}

///
// makeCylinder - Create polygons for a cylinder with unit height, centered at
// the origin, with separate number of radial subdivisions and height
// subdivisions.
//
// @param radius - Radius of the base of the cylinder
// @param radialDivision - number of subdivisions on the radial base
// @param heightDivisions - number of subdivisions along the height
//
// Can only use calls to C.addTriangle()
///
void makeCylinder(Canvas &C, float radius, int radialDivisions,
                  int heightDivisions) {
  if (radialDivisions < 3)
    radialDivisions = 3;

  if (heightDivisions < 1)
    heightDivisions = 1;

  Vertex topCenter = {0, 0.5, 0};
  Vertex bottomCenter = {0, -0.5, 0};
  float angleDiff = (2 * M_PI) / (float)radialDivisions;
  for (float angle = 0; angle < 2 * M_PI; angle += angleDiff) {
    Vertex topV1 = alongCircle(0.5, radius, angle);
    Vertex topV2 = alongCircle(0.5, radius, angle + angleDiff);
    Vertex bottomV1 = alongCircle(-0.5, radius, angle + angleDiff);
    Vertex bottomV2 = alongCircle(-0.5, radius, angle);
    C.addTriangle(topCenter, topV1, topV2);
    C.addTriangle(bottomCenter, bottomV1, bottomV2);

    for (float i = 0; i < heightDivisions; ++i) {
      float nextY = (i + 1) / (float)heightDivisions;
      float thisY = i / (float)heightDivisions;
      Vertex lr = topV2;
      lr.y -= 1.0 - thisY;
      Vertex ur = topV2;
      ur.y -= 1.0 - nextY;
      Vertex ul = topV1;
      ul.y -= 1.0 - nextY;
      Vertex ll = topV1;
      ll.y -= 1.0 - thisY;
      addQuad(C, lr, ur, ul, ll);
    }
  }
}

///
// makeCone - Create polygons for a cone with unit height, centered at the
// origin, with separate number of radial subdivisions and height
// subdivisions.
//
// @param radius - Radius of the base of the cone
// @param radialDivision - number of subdivisions on the radial base
// @param heightDivisions - number of subdivisions along the height
//
// Can only use calls to C.addTriangle()
///
void makeCone(Canvas &C, float radius, int radialDivisions,
              int heightDivisions) {
  if (radialDivisions < 3)
    radialDivisions = 3;

  if (heightDivisions < 1)
    heightDivisions = 1;

  Vertex topCenter = {0, 0.5, 0};
  Vertex bottomCenter = {0, -0.5, 0};

  float angleDiff = (2 * M_PI) / (float)radialDivisions;
  for (float angle = 0; angle < 2 * M_PI; angle += angleDiff) {
    Vertex v1 = alongCircle(-0.5, radius, angle);
    Vertex v2 = alongCircle(-0.5, radius, angle + angleDiff);
    C.addTriangle(bottomCenter, v1, v2);

    auto diff1 = differenceVector(topCenter, v1);
    auto diff2 = differenceVector(topCenter, v2);
    C.addTriangle(topCenter,
                  topCenter + (diff2 / (float)heightDivisions),
                  topCenter + (diff1 / (float)heightDivisions));

    // Start at 1, making quads as we go down.
    for (float h = 1; h < heightDivisions; ++h) {
      auto startDiff1 = (diff1 / (float)heightDivisions) * h;
      auto startDiff2 = (diff2 / (float)heightDivisions) * h;
      auto endDiff1 = (diff1 / (float)heightDivisions) * (h + 1);
      auto endDiff2 = (diff2 / (float)heightDivisions) * (h + 1);

      addQuad(C,
              topCenter + startDiff2,
              topCenter + endDiff2,
              topCenter + endDiff1,
              topCenter + startDiff1);
    }
  }
}

struct Triangle {
  Vertex v1, v2, v3;
};

#pragma mark - Base Icosahedron Mesh

#define A (0.6180339887 / 2)
Vertex v[12] = {
  { 0,  A, -0.5},
  {-A,  0.5,  0},
  { A,  0.5,  0},
  { 0,  A,  0.5},
  {-0.5,  0,  A},
  { 0, -A,  0.5},
  { 0.5,  0,  A},
  { 0.5,  0, -A},
  { 0, -A, -0.5},
  {-0.5,  0, -A},
  {-A, -0.5,  0},
  { A, -0.5,  0}
};
Triangle triangles[20] = {
  {v[0], v[1], v[2]},
  {v[3], v[2], v[1]},
  {v[3], v[4], v[5]},
  {v[3], v[5], v[6]},
  {v[0], v[7], v[8]},
  {v[0], v[8], v[9]},
  {v[5], v[10], v[11]},
  {v[8], v[11], v[10]},
  {v[1], v[9], v[4]},
  {v[10], v[4], v[9]},
  {v[2], v[6], v[7]},
  {v[11], v[7], v[6]},
  {v[3], v[1], v[4]},
  {v[3], v[6], v[2]},
  {v[0], v[9], v[1]},
  {v[0], v[2], v[7]},
  {v[8], v[10], v[9]},
  {v[8], v[7], v[11]},
  {v[5], v[4], v[10]},
  {v[5], v[11], v[6]}
};
#undef A


/**
 Snaps the position of the provided vertex to ensure it's exactly `radius` units
 from the center of the circle.

 @param v The vertex to snap
 @return The point along the surface normal of the vertex that is exactly
         `radius` from the center.
 */
Vertex equidistantFromCenter(Vertex v, float radius) {
  // Calculate the surface normal of the vector, then fix it to the radius.
  float normal = sqrtf(v.x * v.x + v.y * v.y + v.z * v.z) / radius;
  return {v.x / normal, v.y / normal, v.z / normal};
}

Vertex midpoint(Vertex a, Vertex b) {
  return {
    a.x + ((b.x - a.x) * 0.5f),
    a.y + ((b.y - a.y) * 0.5f),
    a.z + ((b.z - a.z) * 0.5f)
  };
}

void subdivideTriangle(Canvas &C, Triangle triangle,
                       float radius, int subdivisions) {
  if (subdivisions == 1) {
    C.addTriangle(equidistantFromCenter(triangle.v1, radius),
                  equidistantFromCenter(triangle.v2, radius),
                  equidistantFromCenter(triangle.v3, radius));
    return;
  }

  auto v1 = triangle.v1;
  auto v2 = triangle.v2;
  auto v3 = triangle.v3;

  /* Divide this triangle into 4 subtriangles. (Bad ASCII art).

     ----------------
     \      /\      /
      \    /  \    /
       \  /    \  /
        \/      \/
         \------/
          \    /
           \  /
            \/
  */
  subdivideTriangle(C,
    {v1, midpoint(v1, v2), midpoint(v1, v3)},
    radius,
    subdivisions - 1
  );
  subdivideTriangle(C,
    {midpoint(v1, v2), v2, midpoint(v2, v3)},
    radius,
    subdivisions - 1
  );
  subdivideTriangle(C,
    {midpoint(v1, v3), midpoint(v2, v3), v3},
    radius,
    subdivisions - 1
  );
  subdivideTriangle(C,
    {midpoint(v1, v3), midpoint(v1, v2), midpoint(v2, v3)},
    radius,
    subdivisions - 1
  );
}

///
// makeSphere - Create sphere of a given radius, centered at the origin,
// using spherical coordinates with separate number of thetha and
// phi subdivisions.
//
// @param radius - Radius of the sphere
// @param slices - number of subdivisions in the theta direction
// @param stacks - Number of subdivisions in the phi direction.
//
// Can only use calls to C.addTriangle()
///
void makeSphere(Canvas &C, float radius, int slices, int stacks) {
  // IF DOING RECURSIVE SUBDIVISION:
  //   use a minimum value of 1 instead of 3
  //   add an 'else' clause to set a maximum value of 5
  slices = std::min(std::max(slices, 1), 5);

  for (auto &tri : triangles) {
    subdivideTriangle(C, tri, radius, slices);
  }
}
