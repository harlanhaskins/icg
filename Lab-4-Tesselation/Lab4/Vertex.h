//
//  Vertex.h
//
//  Representation of a 3D "Vertex".
//
//  Author:  Warren R. Carithers
//  Date:    2016/10/07 12:34:54
//

#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <glm/glm.hpp>

///
// Information pertaining to a vertex
///

struct Vertex {
  float x;
  float y;
  float z;

  Vertex(glm::vec3 &v): x(v.x), y(v.y), z(v.z) {}
  Vertex(float x, float y, float z): x(x), y(y), z(z) {}

  operator glm::vec3() {
    return {x, y, z};
  }

  Vertex operator+(glm::vec3 v) {
    return {x + v.x, y + v.y, z + v.z};
  }

  Vertex operator/ (float f) {
    return {x / f, y / f, z / f};
  }
};

#endif
