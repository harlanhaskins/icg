//
//  Viewing
//
//  Created by Warren R. Carithers 2016/11/11.
//  Based on code created by Joe Geigel on 1/23/13.
//  Copyright 2016 Rochester Institute of Technology.  All rights reserved.
//
//  Simple class for setting up the viewing and projection transforms
//  for the Transformation Assignment.
//
//  This code can be compiled as either C or C++.
//
//  Contributor:  Harlan Haskins
//

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif

#ifndef __APPLE__
#include <GL/glew.h>
#endif

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Viewing.h"

void setUniform(GLuint program, const char *name, glm::mat4 matrix) {
  auto loc = glGetUniformLocation(program, name);
  glUniformMatrix4fv(loc, 1, false, glm::value_ptr(matrix));
}

///
// This function sets up the view and projection parameter for a frustum
// projection of the scene. See the assignment description for the values
// for the projection parameters.
//
// You will need to write this function, and maintain all of the values
// needed to be sent to the vertex shader.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
///
void setUpFrustum( GLuint program )
{
  glm::mat4 frustum = glm::frustum(-1.0, 1.0, -1.0, 1.0, 0.9, 4.5);
  setUniform(program, "projectionTransform", frustum);
}


///
// This function sets up the view and projection parameter for an
// orthographic projection of the scene. See the assignment description
// for the values for the projection parameters.
//
// You will need to write this function, and maintain all of the values
// needed to be sent to the vertex shader.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
///
void setUpOrtho( GLuint program )
{
  glm::mat4 ortho = glm::ortho(-1.0, 1.0, -1.0, 1.0, 0.9, 4.5);
  setUniform(program, "projectionTransform", ortho);
}


///
// This function clears any model transformations, setting the values
// to the defaults: no scaling (scale factor of 1), no rotation (degree
// of rotation = 0), and no translation (0 translation in each direction).
//
// You will need to write this function, and maintain all of the values
// which must be sent to the vertex shader.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
///
void clearTransforms( GLuint program )
{
  setUniform(program, "modelTransform", glm::mat4(1));
}


///
// This function sets up the transformation parameters for the vertices
// of the teapot.  The order of application is specified in the driver
// program.
//
// You will need to write this function, and maintain all of the values
// which must be sent to the vertex shader.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
// @param scaleX - amount of scaling along the x-axis
// @param scaleY - amount of scaling along the y-axis
// @param scaleZ - amount of scaling along the z-axis
// @param rotateX - angle of rotation around the x-axis, in degrees
// @param rotateY - angle of rotation around the y-axis, in degrees
// @param rotateZ - angle of rotation around the z-axis, in degrees
// @param translateX - amount of translation along the x axis
// @param translateY - amount of translation along the y axis
// @param translateZ - amount of translation along the z axis
///
void setUpTransforms( GLuint program,
    GLfloat scaleX, GLfloat scaleY, GLfloat scaleZ,
    GLfloat rotateX, GLfloat rotateY, GLfloat rotateZ,
    GLfloat translateX, GLfloat translateY, GLfloat translateZ )
{

  auto radsX = glm::radians(rotateX);
  auto radsY = glm::radians(rotateY);
  auto radsZ = glm::radians(rotateZ);

  glm::mat4 rotateXTransform(
    1,  0,          0,          0,
    0,  cos(radsX), sin(radsX), 0,
    0, -sin(radsX), cos(radsX), 0,
    0,  0,          0,          1
  );
  glm::mat4 rotateYTransform(
    cos(radsY), 0, -sin(radsY), 0,
    0,          1,  0,          0,
    sin(radsY), 0,  cos(radsY), 0,
    0,          0,  0,          1
  );
  glm::mat4 rotateZTransform(
     cos(radsZ), sin(radsZ), 0, 0,
    -sin(radsZ), cos(radsZ), 0, 0,
     0,          0,          1, 0,
     0,          0,          0, 1
  );

  glm::mat4 rotateTransform =
    rotateYTransform * rotateXTransform * rotateZTransform;

  glm::mat4 scaleTransform(
    scaleX, 0,      0,      0,
    0,      scaleY, 0,      0,
    0,      0,      scaleZ, 0,
    0,      0,      0,      1
  );

  glm::mat4 translateTransform(
    1,          0,          0,          0,
    0,          1,          0,          0,
    0,          0,          1,          0,
    translateX, translateY, translateZ, 1
  );

  glm::mat4 finalTransform =
    translateTransform * rotateTransform * scaleTransform;

  setUniform(program, "modelTransform", finalTransform);
}

///
// This function clears any changes made to camera parameters, setting the
// values to the defaults: eye (0.0,0.0,0.0), lookat (0,0,0.0,-1.0),
// and up vector (0.0,1.0,0.0).
//
// You will need to write this function, and maintain all of the values
// which must be sent to the vertex shader.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
///
void clearCamera( GLuint program )
{
  setUniform(program, "viewTransform", glm::mat4(1));
}

///
// This function sets up the camera parameters controlling the viewing
// transformation.
//
// You will need to write this function, and maintain all of the values
// which must be sent to the vertex shader.
//
// @param program - The ID of an OpenGL (GLSL) shader program to which
//    parameter values are to be sent
// @param eyeX - x coordinate of the camera location
// @param eyeY - y coordinate of the camera location
// @param eyeZ - z coordinate of the camera location
// @param lookatX - x coordinate of the lookat point
// @param lookatY - y coordinate of the lookat point
// @param lookatZ - z coordinate of the lookat point
// @param upX - x coordinate of the up vector
// @param upY - y coordinate of the up vector
// @param upZ - z coordinate of the up vector
///
void setUpCamera( GLuint program,
    GLfloat eyeX, GLfloat eyeY, GLfloat eyeZ,
    GLfloat lookatX, GLfloat lookatY, GLfloat lookatZ,
    GLfloat upX, GLfloat upY, GLfloat upZ )
{
  glm::vec3 eye(eyeX, eyeY, eyeZ);
  glm::vec3 lookat(lookatX, lookatY, lookatZ);
  glm::vec3 up(upX, upY, upZ);

  // glm::lookAt is a very optimized version of the standard camera
  // viewing transform:
  // ux uy uz -1(u * eyepoint)
  // vx vy vz -1(v * eyepoint)
  // nx ny nz -1(n * eyepoint)
  setUniform(program, "viewTransform", glm::lookAt(eye, lookat, up));
}
