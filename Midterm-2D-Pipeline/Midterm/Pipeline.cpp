//
//  Pipeline.cpp
//
//  Created by Warren R. Carithers on 2016/10/19.
//  Based on a version created by Joe Geigel on 11/30/11.
//  Copyright 2016 Rochester Institute of Technology. All rights reserved.
//
//  Contributor:  YOUR_NAME_HERE
//

#include <cmath>
#include <iostream>
#include <utility>

#include "Pipeline.h"
#include "Rasterizer.h"

///
// Simple wrapper class for midterm assignment
//
// Only methods in the implementation file whose bodies
// contain the comment
//
//    // YOUR IMPLEMENTATION HERE
//
// are to be modified by students.
///

///
// Constructor
//
// @param w width of canvas
// @param h height of canvas
///
Pipeline::Pipeline(int w, int h) : Canvas(w, h) {}

///
// addPoly - Add a polygon to the canvas.  This method does not draw
//           the polygon, but merely stores it for later drawing.
//           Drawing is initiated by the drawPoly() method.
//
//           Returns a unique integer id for the polygon.
//
// @param p - Array containing the vertices of the polygon to be added.
// @param n - Number of vertices in polygon
//
// @return a unique integer identifier for the polygon
///
int Pipeline::addPoly(const Vertex p[], int n) {
  auto idx = polygons.size();
  polygons.emplace_back(std::vector<Vertex>());
  for (auto i = 0; i < n; ++i) {
    polygons.back().vertices.push_back(p[i]);
  }
  return (int)idx;
}

///
// drawPoly - Draw the polygon with the given id.  The polygon should
//            be drawn after applying the current transformation to
//            the vertices of the polygon.
//
// @param polyID - the ID of the polygon to be drawn.
///
void Pipeline::drawPoly(int polyID) {
  auto &polygon = polygons[polyID];
  std::vector<Vertex> vertices;

  Clipper c;
  c.clipPolygon(polygon.vertices, vertices, clipLL, clipUR);

  auto pipeline = viewTransform * normalizationTransform * mainTransform;

  for (auto &vertex : vertices) {
    glm::vec3 tmp(vertex.x, vertex.y, 1);
    tmp = pipeline * tmp;
    vertex.x = tmp.x / tmp.z;
    vertex.y = tmp.y / tmp.z;
  }

  Rasterizer r(this->height, *this);
  r.drawPolygon(vertices);
}

///
// clearTransform - Set the current transformation to the identity matrix.
///
void Pipeline::clearTransform(void) {
  mainTransform = glm::mat3x3(1);
}

///
// translate - Add a translation to the current transformation by
//             premultiplying the appropriate translation matrix to
//             the current transformation matrix.
//
// @param x - Amount of translation in x.
// @param y - Amount of translation in y.
///
void Pipeline::translate(float tx, float ty) {
  mainTransform = glm::transpose(glm::mat3x3(
    1, 0, tx,
    0, 1, ty,
    0, 0, 1
  )) * mainTransform;
}

///
// rotate - Add a rotation to the current transformation by premultiplying
//          the appropriate rotation matrix to the current transformation
//          matrix.
//
// @param degrees - Amount of rotation in degrees.
///
void Pipeline::rotate(float degrees) {
  auto rads = glm::radians(degrees);
  mainTransform = glm::transpose(glm::mat3x3(
    cos(rads), -sin(rads), 0,
    sin(rads),  cos(rads), 0,
    0,          0,         1
  )) * mainTransform;
}

///
// scale - Add a scale to the current transformation by premultiplying
//         the appropriate scaling matrix to the current transformation
//         matrix.
//
// @param x - Amount of scaling in x.
// @param y - Amount of scaling in y.
///
void Pipeline::scale(float sx, float sy) {
  mainTransform = glm::transpose(glm::mat3x3(
    sx, 0,  0,
    0,  sy, 0,
    0,  0,  1)) * mainTransform;
}

///
// setClipWindow - Define the clip window.
//
// @param bottom - y coord of bottom edge of clip window (in world coords)
// @param top - y coord of top edge of clip window (in world coords)
// @param left - x coord of left edge of clip window (in world coords)
// @param right - x coord of right edge of clip window (in world coords)
///
void Pipeline::setClipWindow(float bottom, float top, float left, float right) {
  clipLL = {bottom, left};
  clipUR = {top, right};
  normalizationTransform = glm::transpose(glm::mat3x3(
      2 / (right - left), 0,                  ((-2 * left) / (right - left)) - 1,
      0,                  2 / (top - bottom), ((-2 * bottom) / (top - bottom)) - 1,
      0,                  0,                  1
  ));
}

///
// setViewport - Define the viewport.
//
// @param xmin - x coord of lower left of view window (in screen coords)
// @param ymin - y coord of lower left of view window (in screen coords)
// @param width - width of view window (in world coords)
// @param height - width of view window (in world coords)
///
void Pipeline::setViewport(int x, int y, int width, int height) {
  viewTransform = glm::transpose(glm::mat3x3(
    width / 2, 0,          ((2 * x) + width) / 2,
    0,         height / 2, ((2 * y) + height) / 2,
    0,         0,          1
  ));
}
