//
//  Clipper.cpp
//
//  Created by Warren R. Carithers on 01/15/14.
//  Based on a version created by Joe Geigel on 11/30/11.
//  Copyright 2014 Rochester Institute of Technology. All rights reserved.
//
//  Contributor:  Harlan Haskins
//

#include "Clipper.h"
#include "Vertex.h"
#include <vector>

struct Edge {
  enum class Position : uint8_t { Left, Right, Bottom, Top };
  const Vertex start, end;
  const Position position;
  Edge(const Vertex start, const Vertex end, const Position position)
      : start(start), end(end), position(position) {}

  bool contains(const Vertex &v) {
    switch (position) {
    case Position::Left:
      return v.x >= start.x;
    case Position::Right:
      return v.x <= start.x;
    case Position::Bottom:
      return v.y >= start.y;
    case Position::Top:
      return v.y <= start.y;
    }
  }
};

///
// Simple module that performs clipping
///

Vertex computeIntersection(const Vertex &start, const Vertex &end,
                           const Edge &edge) {
  // Compute the intersection of the two line segments by converting them
  // into an ad-hoc matrix structure, computing their determinant, and
  // projecting the first line onto the second.
  //
  // This algorithm assumes the determinant of two lines will be non-zero.
  // Equation is from:
  // https://math.stackexchange.com/questions/25171/intersection-of-two-lines-in-2d
  auto a1 = end.y - start.y;
  auto b1 = start.x - end.x;
  auto c1 = a1 * (start.x) + b1 * (start.y);

  auto a2 = edge.end.y - edge.start.y;
  auto b2 = edge.start.x - edge.end.x;
  auto c2 = a2 * (edge.start.x) + b2 * (edge.start.y);

  auto determinant = a1 * b2 - a2 * b1;

  auto x = (b2 * c1 - b1 * c2) / determinant;
  auto y = (a1 * c2 - a2 * c1) / determinant;
  return {x, y};
}

///
// clipPolygons
//
// Clip the polygon with vertex count in and vertices inV against the
// rectangular clipping region specified by lower-left corner ll and
// upper-right corner ur. The resulting vertices are placed in outV.
//
// The routine should return the with the vertex count of polygon
// resulting from the clipping.
//
// @param in    the number of vertices in the polygon to be clipped
// @param inV   the incoming vertex list
// @param outV  the outgoing vertex list
// @param ll    the lower-left corner of the clipping rectangle
// @param ur    the upper-right corner of the clipping rectangle
//
// @return number of vertices in the polygon resulting after clipping
//
///
void Clipper::clipPolygon(const std::vector<Vertex> &inputEdges,
                          std::vector<Vertex> &outputEdges, Vertex ll,
                          Vertex ur) {
  Edge edges[4] = {
      Edge(ll, {ll.x, ur.y}, Edge::Position::Left),
      Edge(ll, {ur.x, ll.y}, Edge::Position::Bottom),
      Edge({ur.x, ll.y}, ur, Edge::Position::Right),
      Edge({ll.x, ur.y}, ur, Edge::Position::Top),
  };

  // The Sutherland-Hodgman Clipping Algorithm.

  // Start by making an output vector containing, at first, all the vertices
  // of the polygon.
  outputEdges = inputEdges;

  // For each clipping edge, compute the set of vertices that comprise the inner
  // polygon formed by the intersections of each original polygon with this
  // current clip edge.
  for (auto &edge : edges) {
    // Copy the current set of output edges, and clear the output set. We're
    // going to only add the edges that have not been clipped from this edge.
    auto inputList = outputEdges;
    outputEdges.clear();

    // Starting from the most recently added edge (assuming counter-clockwise),
    // determine which portions of a line are inside or outside a clip region.
    // We do this by iterating over pairs of vertices, starting with
    // (last, first)
    auto prev = inputList.back();
    for (auto &curr : inputList) {
      // If the line segment crosses the clip edge (one is in and the other
      // is not), add their intersection to the set of output edges.
      if (edge.contains(curr) != edge.contains(prev)) {
        Vertex inter = computeIntersection(prev, curr, edge);
        outputEdges.push_back(inter);
      }
      if (edge.contains(curr)) {
        // Add the first edge that we know is inside the clip window.
        outputEdges.push_back(curr);
      }
      // Skip to the next edge, so we get (curr, next) as the edges to check,
      // and repeat.
      prev = curr;
    }
  }
}
