//
//  Rasterizer.h
//
//  Created by Joe Geigel on 11/30/11.
//  Modifications by Warren R. Carithers.
//  Copyright 2016 Rochester Institute of Technology. All rights reserved.
//
//  Contributor:  YOUR_NAME_HERE
//

#ifndef _RASTERIZER_H_
#define _RASTERIZER_H_

#include "Vertex.h"

class Canvas;

///
// Simple class that performs rasterization algorithms
///

class Rasterizer {

  ///
  // number of scanlines
  ///

  int n_scanlines;

public:
  ///
  // Drawing canvas
  ///

  Canvas &C;

  ///
  // Constructor
  //
  // @param n number of scanlines
  // @param C The Canvas to use
  ///
  Rasterizer(int n, Canvas &canvas);

  ///
  // Draw a filled polygon
  //
  // Implementation should use the scan-line polygon fill algorithm
  // discussed in class.
  //
  // You are to add the implementation here using only calls to the
  // setPixel() method of the canvas.
  //
  // @param vertices A vector of vertices making up the polygon.
  ///
  void drawPolygon(const std::vector<Vertex> &vertices);
};

#endif
