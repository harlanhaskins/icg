//
//  Rasterizer.cpp
//
//  Created by Joe Geigel on 11/30/11.
//  Modifications by Warren R. Carithers.
//  Copyright 2011 Rochester Institute of Technology. All rights reserved.
//
//  Contributor: Harlan Haskins
//

#include <vector>
#include <algorithm>
#include <cmath>
#include <iostream>

#include "Rasterizer.h"
#include "Canvas.h"

///
// Simple class that performs rasterization algorithms
///

/// Represents an edge in an edge table.
struct PolygonEdge {
  /// The highest Y value in the edge.
  int yMax;

  /// The current X position being set in the canvas. This is a floating point
  /// value because we increment it by the inverseSlope.
  float x;

  /// The inverse of the slope of this line, used to determine how much in the
  /// X direction to move on each scanline.
  float inverseSlope;

  /// Creates a new Edge representing the provided vertices.
  PolygonEdge(int x0, int y0, int x1, int y1) {
    // If our edge is given right-to-left, swap the vertices to ensure we're
    // always progressing left-to-right.
    if (x1 < x0) {
      std::swap(x0, x1);
      std::swap(y0, y1);
    }

    // Find the highest Y coordinate.
    yMax = std::max(y0, y1);

    // Find the X cooresponding to the lowest Y coordinate (where we will begin
    // drawing.)
    x = y0 < y1 ? x0 : x1;

    // Compute the inverse slope.
    inverseSlope = (float)(x1 - x0) / (float) (y1 - y0);
  }

  int roundedX() {
    return inverseSlope == 0 ? x : ceil(x);
  }
};

///
// Constructor
//
// @param n number of scanlines
// @param C The Canvas to use
///
Rasterizer::Rasterizer( int n, Canvas &canvas ) : n_scanlines(n), C(canvas)
{
}

///
// Draw a filled polygon.
//
// Implementation should use the scan-line polygon fill algorithm
// discussed in class.
//
// The polygon has n distinct vertices.  The coordinates of the vertices
// making up the polygon are stored in the x and y arrays.  The ith
// vertex will have coordinate (x[i],y[i]).
//
// You are to add the implementation here using only calls to the
// setPixel() function.
//
// @param n - number of vertices
// @param x - array of x coordinates
// @param y - array of y coordinates
//
// @note: This algorithm comes from
// http://www.cad.zju.edu.cn/home/zhx/CG/2016/lib/exe/fetch.php?media=fillalgorithm.pdf
///
void Rasterizer::drawPolygon(const std::vector<Vertex> &vertices) {
  if (vertices.size() < 3) return;

  // Compute the lower and upper scanlines for the object. This will tell us
  // the number of scanlines we have total.
  int minY = vertices[0].y;
  int maxY = vertices[0].y;
  for (auto &vertex : vertices) {
    minY = std::min(minY, (int)vertex.y);
    maxY = std::max(maxY, (int)vertex.y);
  }

  // Create one edge bucket per scanline in the polygon.
  std::vector<PolygonEdge> edgeBuckets[maxY-minY+1];

  // Populate the edge table with alternating pairs of vertices, looping back
  // around to close the polygon with the first and last vertices.
  for (size_t i = 0; i < vertices.size(); ++i) {
    auto nextIdx = (i + 1) % vertices.size();
    auto &v0 = vertices[i];
    auto &v1 = vertices[nextIdx];
    int x0 = v0.x, y0 = v0.y;
    int x1 = v1.x, y1 = v1.y;

    // Ignore horizontal edges.
    if (y0 == y1) continue;

    // Get the index of the bucket (which is the difference from the smallest
    // scanline to the current scanline).
    int bucketIndex = std::min(y0, y1) - minY;

    // Append the new edge to the bucket.
    edgeBuckets[bucketIndex].emplace_back(x0, y0, x1, y1);
  }

  // Create an active list of edges that intersect the scan line.
  std::vector<PolygonEdge *> activeList;

  int lastDrawnScanline = 0;

  // For each scanline that this polygon intersects...
  for (int scanLine = minY; scanLine <= maxY; ++scanLine) {
    // Remove anything in the active list if the current scan line is past its
    // yMax.
    auto removeIter =
      std::remove_if(activeList.begin(), activeList.end(),
                     [&](PolygonEdge *e) { return e->yMax == scanLine; });
    activeList.erase(removeIter, activeList.end());

    // Add all the edges that begin on this scanline to the activeList.
    int bucketIdx = scanLine - minY;
    auto &edges = edgeBuckets[bucketIdx];
    for (auto &edge : edges) {
      activeList.push_back(&edge);
    }

    // If we've added any edges, sort the current activeList by x positions,
    // and secondarily by slopes.
    if (!edges.empty()) {
      std::sort(activeList.begin(), activeList.end(),
                [](PolygonEdge *e1, PolygonEdge *e2) {
                  if (e1->x < e2->x) return true;
                  return e1->x == e2->x && e1->inverseSlope < e2->inverseSlope;
                });
    }

    // Draw each pixel between the left edge and right edge.
    if (!activeList.empty()) {
      for (int i = 0; i < activeList.size() - 1; i += 2) {
        auto start = activeList[i];
        auto end = activeList[i + 1];
        for (int x = start->roundedX(); x < end->roundedX(); ++x) {
          lastDrawnScanline = scanLine;
          C.setPixel(x, scanLine);
        }
      }
    }

    // Increment the x of each active edge with the inverse slope.
    for (auto *edge : activeList) {
      edge->x += edge->inverseSlope;
    }
  }
}
