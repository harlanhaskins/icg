#ifndef _UTILS_H_
#define _UTILS_H_
#include <glm/gtx/string_cast.hpp>

std::ostream& operator<<(std::ostream& out, const glm::mat4& g);
std::ostream& operator<<(std::ostream& out, const glm::mat3& g);
std::ostream& operator<<(std::ostream& out, const glm::vec4& g);
std::ostream& operator<<(std::ostream& out, const glm::vec3& g);

namespace std {
  template <class T, typename H>
  inline void hash_combine(H& seed, const T& v) {
    seed ^= std::hash<T>{}(v) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
  }
}

#endif /* _UTILS_H_ */
