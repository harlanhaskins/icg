#include "Mesh.h"
#include "Shader.h"
#include <GLFW/glfw3.h>

using namespace glm;

std::shared_ptr<Mesh> Mesh::create() {
  // Cannot use std::make_shared because default constructor is private.
  return std::shared_ptr<Mesh>(new Mesh());
}

void Mesh::addVertex(Vertex vert) {
  auto iter = indexTable.find(vert.position);
  if (iter != indexTable.end()) {
    elements.push_back(iter->second);
    return;
  }
  auto size = (GLuint)vertexPositions.size();
  elements.emplace_back(size);
  indexTable[vert] = size;
  vertexPositions.emplace_back(vert.position);
  vertexNormals.emplace_back(vert.normal);
  vertexTextureCoordinates.emplace_back(vert.textureCoordinate);
}

void Mesh::addTriangle(Triangle triangle, bool inferNormals) {
  if (inferNormals) {
    triangle.computeNormals();
  }
  for (auto &v : triangle.vertices) {
    addVertex(v);
  }
}

#pragma mark - Base Icosahedron Mesh

#define A (0.6180339887 / 2)
vec3 v[12] = {
  { 0,  A, -0.5},
  {-A,  0.5,  0},
  { A,  0.5,  0},
  { 0,  A,  0.5},
  {-0.5,  0,  A},
  { 0, -A,  0.5},
  { 0.5,  0,  A},
  { 0.5,  0, -A},
  { 0, -A, -0.5},
  {-0.5,  0, -A},
  {-A, -0.5,  0},
  { A, -0.5,  0}
};
Triangle triangles[20] = {
  {v[0], v[1], v[2]},
  {v[3], v[2], v[1]},
  {v[3], v[4], v[5]},
  {v[3], v[5], v[6]},
  {v[0], v[7], v[8]},
  {v[0], v[8], v[9]},
  {v[5], v[10], v[11]},
  {v[8], v[11], v[10]},
  {v[1], v[9], v[4]},
  {v[10], v[4], v[9]},
  {v[2], v[6], v[7]},
  {v[11], v[7], v[6]},
  {v[3], v[1], v[4]},
  {v[3], v[6], v[2]},
  {v[0], v[9], v[1]},
  {v[0], v[2], v[7]},
  {v[8], v[10], v[9]},
  {v[8], v[7], v[11]},
  {v[5], v[4], v[10]},
  {v[5], v[11], v[6]}
};
#undef A

/// I spent so much time on this.
void computeSphericalAttrs(Vertex &v) {
  v.normal = v.position;

  vec3 pos = v.position;
  
  float theta = atan2(pos.z, pos.x);
  if (pos.z < 0) {
    theta += (2*M_PI);
  }
  v.textureCoordinate.x = theta / (2*M_PI);

  float phi = asin(pos.y);
  v.textureCoordinate.y = (phi / M_PI) + 0.5;
}

Vertex midpoint(Vertex aVertex, Vertex bVertex) {
  vec3 a = aVertex.position;
  vec3 b = bVertex.position;
  return vec3(
    a.x + ((b.x - a.x) * 0.5f),
    a.y + ((b.y - a.y) * 0.5f),
    a.z + ((b.z - a.z) * 0.5f)
  );
}

void subdivideTriangle(Mesh &M, Triangle triangle, int subdivisions) {
  auto v1 = triangle.vertices[0];
  auto v2 = triangle.vertices[1];
  auto v3 = triangle.vertices[2];

  if (subdivisions == 1) {
    Vertex t1 = normalize(v1.position);
    computeSphericalAttrs(t1);
    Vertex t2 = normalize(v2.position);
    computeSphericalAttrs(t2);
    Vertex t3 = normalize(v3.position);
    computeSphericalAttrs(t3);
    M.addTriangle({t1, t2, t3});
    return;
  }

  /* Divide this triangle into 4 subtriangles. (Bad ASCII art).

   ----------------
   \      /\      /
    \    /  \    /
     \  /    \  /
      \/      \/
       \------/
        \    /
         \  /
          \/
   */
  subdivideTriangle(M,
    {v1, midpoint(v1, v2), midpoint(v1, v3)},
    subdivisions - 1
  );
  subdivideTriangle(M,
    {midpoint(v1, v2), v2, midpoint(v2, v3)},
    subdivisions - 1
  );
  subdivideTriangle(M,
    {midpoint(v1, v3), midpoint(v2, v3), v3},
    subdivisions - 1
  );
  subdivideTriangle(M,
    {midpoint(v1, v3), midpoint(v1, v2), midpoint(v2, v3)},
    subdivisions - 1
  );
}

///
// makeSphere - Create sphere of a given radius, centered at the origin,
// using spherical coordinates with separate number of thetha and
// phi subdivisions.
//
// @param radius - Radius of the sphere
// @param slices - number of subdivisions in the theta direction
// @param stacks - Number of subdivisions in the phi direction.
///
std::shared_ptr<Mesh> Mesh::sphere(int subdivisions) {
  auto m = Mesh::create();
  subdivisions = std::min(std::max(subdivisions, 1), 7);

  for (auto &tri : triangles) {
    subdivideTriangle(*m, tri, subdivisions);
  }

  return m;
}

static Vertex alongCircle(float y, float radius, float rads) {
  return Vertex({ sin(rads) * radius, y, cos(rads) * radius });
}

//
// makeCylinder - Create polygons for a cylinder with unit height and diameter,
//                centered at
//                the origin, with separate number of radial subdivisions and
//                height subdivisions.
//
// @param radialDivision - number of subdivisions on the radial base
// @param heightDivisions - number of subdivisions along the height
//
//
std::shared_ptr<Mesh>
Mesh::cylinder(int radialDivisions, int heightDivisions) {
  if (radialDivisions < 3)
    radialDivisions = 3;

  if (heightDivisions < 1)
    heightDivisions = 1;

  auto m = Mesh::create();
  float radius = 0.5;

  Vertex topCenter({0, 0.5, 0});
  Vertex bottomCenter({0, -0.5, 0});
  float angleDiff = (2 * M_PI) / (float)radialDivisions;
  for (float angle = 0; angle < 2 * M_PI; angle += angleDiff) {
    Vertex topV1 = alongCircle(0.5, radius, angle);
    Vertex topV2 = alongCircle(0.5, radius, angle + angleDiff);
    Vertex bottomV1 = alongCircle(-0.5, radius, angle + angleDiff);
    Vertex bottomV2 = alongCircle(-0.5, radius, angle);
    m->addTriangle({topCenter, topV1, topV2});
    m->addTriangle({bottomCenter, bottomV1, bottomV2});

    for (float i = 0; i < heightDivisions; ++i) {
      float nextY = (i + 1) / (float)heightDivisions;
      float thisY = i / (float)heightDivisions;
      Vertex lr = topV2;
      lr.position.y -= 1.0 - thisY;
      lr.textureCoordinate = {
        0.5 + atan2(lr.position.z, lr.position.y),
        0.5 - asin(lr.position.y)
      };
      Vertex ur = topV2;
      ur.position.y -= 1.0 - nextY;
      Vertex ul = topV1;
      ul.position.y -= 1.0 - nextY;
      Vertex ll = topV1;
      ll.position.y -= 1.0 - thisY;
      m->addQuad(lr, ur, ul, ll);
    }
  }
  return m;
}

///
// makeCube - Create a unit cube, centered at the origin, with a given number
// of subdivisions in each direction on each face.
//
// @param subdivision - number of equal subdivisions to be made in each
//        direction along each face
//
// Can only use calls to C.addTriangle()
///
std::shared_ptr<Mesh> Mesh::cube() {
  auto m = Mesh::create();
  const float l = 0.5;
  // front face
  m->addQuad(
    { l, -l, -l},
    { l,  l, -l},
    {-l,  l, -l},
    {-l, -l, -l},
    true
  );

  // back face
  m->addQuad(
    {-l, -l, l},
    {-l,  l, l},
    { l,  l, l},
    { l, -l, l},
    true
  );

  // left face
  m->addQuad(
    {-l, -l, -l},
    {-l,  l, -l},
    {-l,  l,  l},
    {-l, -l,  l},
    true
  );

  // right face
  m->addQuad(
    {l, -l,  l},
    {l,  l,  l},
    {l,  l, -l},
    {l, -l, -l},
    true
  );

  // top face
  m->addQuad(
    { l, l, -l},
    { l, l,  l},
    {-l, l,  l},
    {-l, l, -l}
  );

  // bottom face
  m->addQuad(
    { l, -l,  l},
    { l, -l, -l},
    {-l, -l, -l},
    {-l, -l,  l},
    true
  );
  return m;
}

//
// makeCone - Create polygons for a cone with unit height and diameter, centered
//            at the origin, with separate number of radial subdivisions and
//            height subdivisions.
//
// @param radialDivision - number of subdivisions on the radial base
// @param heightDivisions - number of subdivisions along the height
//
//
std::shared_ptr<Mesh>
Mesh::cone(int radialDivisions, int heightDivisions) {
  if (radialDivisions < 3)
    radialDivisions = 3;

  if (heightDivisions < 1)
    heightDivisions = 1;

  auto m = Mesh::create();
  float radius = 0.5;

  Vertex topCenter({0, 0.5, 0});
  Vertex bottomCenter({0, -0.5, 0});

  float angleDiff = (2 * M_PI) / (float)radialDivisions;
  for (float angle = 0; angle < 2 * M_PI; angle += angleDiff) {
    Vertex v1 = alongCircle(-0.5, radius, angle);
    Vertex v2 = alongCircle(-0.5, radius, angle + angleDiff);
    m->addTriangle({bottomCenter, v1, v2});

    auto diff1 = v1.position - topCenter.position;
    auto diff2 = v2.position - topCenter.position;
    m->addTriangle({
      topCenter,
      topCenter + (diff2 / (float)heightDivisions),
      topCenter + (diff1 / (float)heightDivisions)
    });

    // Start at 1, making quads as we go down.
    for (float h = 1; h < heightDivisions; ++h) {
      auto startDiff1 = (diff1 / (float)heightDivisions) * h;
      auto startDiff2 = (diff2 / (float)heightDivisions) * h;
      auto endDiff1 = (diff1 / (float)heightDivisions) * (h + 1);
      auto endDiff2 = (diff2 / (float)heightDivisions) * (h + 1);

      m->addQuad(
        topCenter + startDiff2,
        topCenter + endDiff2,
        topCenter + endDiff1,
        topCenter + startDiff1
      );
    }
  }
  return m;
}

void Mesh::addQuad(glm::vec3 lr, glm::vec3 ur, glm::vec3 ul, glm::vec3 ll, bool inferNormals) {
  return addQuad(
    Vertex(lr, {}, {1, 1}),
    Vertex(ur, {}, {1, 0}),
    Vertex(ul, {}, {0, 0}),
    Vertex(ll, {}, {0, 1}),
    inferNormals
  );
}

void Mesh::addQuad(Vertex lr, Vertex ur, Vertex ul, Vertex ll, bool inferNormals) {
  addTriangle({lr, ur, ul}, inferNormals);
  addTriangle({lr, ul, ll}, inferNormals);
}

static GLuint makeBuffer(GLenum target, const void *data, size_t size) {
  GLuint buf;
  glGenBuffers(1, &buf);
  glBindBuffer(target, buf);
  glBufferData(target, size, data, GL_STATIC_DRAW);
  return buf;
}

template <typename T>
static GLsizei vsizeof(const std::vector<T> &elts) {
  return (GLsizei)elts.size() * sizeof(T);
}

template <typename T>
static void setArraySubdata(
                            GLuint program, const char *name,
                            GLintptr &offset, const std::vector<T> &data) {
  auto totalSize = vsizeof(data);
  glBufferSubData(GL_ARRAY_BUFFER, offset, totalSize, data.data());
  offset += totalSize;
}

template <typename T>
static void enableVertexArray(
  GLuint program, const char *name,
  GLintptr &offset, const std::vector<T> &data) {
  auto totalSize = vsizeof(data);
  auto loc = glGetAttribLocation(program, name);
  glVertexAttribPointer(loc, sizeof(T) / sizeof(float),
                        GL_FLOAT, GL_FALSE, sizeof(T), (void *)offset);
  glEnableVertexAttribArray(loc);
  offset += totalSize;
}

void Mesh::draw(Shader &shader) {
  glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBuffer);
  GLintptr offset = 0;
  auto prog = shader.getProgram();
  enableVertexArray(prog, "vPosition", offset, vertexPositions);
  enableVertexArray(prog, "vNormal", offset, vertexNormals);
  enableVertexArray(prog, "vTexCoord", offset, vertexTextureCoordinates);
  glDrawElements(GL_TRIANGLES, (GLsizei)elements.size(), GL_UNSIGNED_INT, 0);
}

void Mesh::finalize(Shader &shader) {
  if (vertexBuffer != 0) return;

  auto posSize = vsizeof(vertexPositions);
  auto normSize = vsizeof(vertexNormals);
  auto textSize = vsizeof(vertexTextureCoordinates);
  auto totalSize = posSize + normSize + textSize;

  vertexBuffer = makeBuffer(GL_ARRAY_BUFFER, NULL, totalSize);

  GLintptr offset = 0;
  auto prog = shader.getProgram();
  setArraySubdata(prog, "vPosition", offset, vertexPositions);
  setArraySubdata(prog, "vNormal", offset, vertexNormals);
  setArraySubdata(prog, "vTexCoord", offset, vertexTextureCoordinates);

  elementBuffer =
    makeBuffer(GL_ELEMENT_ARRAY_BUFFER, elements.data(), vsizeof(elements));
}
