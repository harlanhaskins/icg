#include "Transform.h"
#include <glm/gtc/matrix_transform.hpp>

glm::mat4 scaleTransform(glm::vec3 scale) {
  return {
    scale.x, 0,       0,       0,
    0,       scale.y, 0,       0,
    0,       0,       scale.z, 0,
    0,       0,       0,       1
  };
}

glm::mat4 translationTransform(glm::vec3 translate) {
  return {
    1,           0,           0,           0,
    0,           1,           0,           0,
    0,           0,           1,           0,
    translate.x, translate.y, translate.z, 1
  };
}

glm::mat4 rotationTransform(glm::vec3 rot) {
  glm::mat4 rotateXTransform(
    1,  0,          0,          0,
    0,  cos(rot.x), sin(rot.x), 0,
    0, -sin(rot.x), cos(rot.x), 0,
    0,  0,          0,          1
  );
  glm::mat4 rotateYTransform(
    cos(rot.y), 0, -sin(rot.y), 0,
    0,          1,  0,          0,
    sin(rot.y), 0,  cos(rot.y), 0,
    0,          0,  0,          1
  );
  glm::mat4 rotateZTransform(
     cos(rot.z), sin(rot.z), 0, 0,
    -sin(rot.z), cos(rot.z), 0, 0,
     0,          0,          1, 0,
     0,          0,          0, 1
  );
  return rotateYTransform * rotateXTransform * rotateZTransform;
}

glm::mat4 Transform::getMatrix() {
  return translationTransform(translation) *
         rotationTransform(rotation) *
         scaleTransform(scalar);
}

void Transform::scaleBy(glm::vec3 scale) {
  scalar *= scale;
}

void Transform::translateBy(glm::vec3 translate) {
  translation += translate;
}

void Transform::rotateBy(glm::vec3 rot) {
  rotation += rot;
}

void Transform::rotateByDegrees(glm::vec3 rotation) {
  rotateBy(glm::radians(rotation));
}

void Transform::setTranslation(glm::vec3 translation) {
  this->translation = translation;
}

void Transform::setScale(glm::vec3 scalar) {
  this->scalar = scalar;
}

void Transform::setRotation(glm::vec3 rotation) {
  this->rotation = rotation;
}

void Transform::setRotationDegrees(glm::vec3 rotation) {
  this->rotation = glm::radians(rotation);
}
