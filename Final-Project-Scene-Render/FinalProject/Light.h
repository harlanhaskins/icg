#ifndef _LIGHT_H_
#define _LIGHT_H_

#include <glm/glm.hpp>

/// Represents a point light source.
struct Light {
  /// The color of the light itself.
  glm::vec3 color;

  /// The position of the light, in scene coordinates.
  glm::vec3 position;
};

#endif /* _LIGHT_H_ */
