#include "Material.h"

void Material::setProperties() {
  shader->setVec3("materialColor", color);
  shader->setFloat("ambientCoeff", ambient);
  shader->setFloat("diffuseCoeff", diffuse);
  shader->setFloat("specCoeff", specular);
  shader->setFloat("specExp", specularExponent);

  shader->setBool("useTexture", texture != nullptr);
  if (texture) {
    texture->bind(*shader, "texture");
    shader->setVec2("textureScaling", textureScaling);
  }
}

std::shared_ptr<Material> Material::withColor(glm::vec3 color) {
  auto mat = Material::create(shader);
  *mat = *this;
  mat->color = color;
  return mat;
}

std::shared_ptr<Material>
Material::withTexture(std::shared_ptr<Texture> texture) {
  auto mat = Material::create(shader);
  *mat = *this;
  mat->texture = texture;
  return mat;
}
