#include "Vertex.h"

bool operator==(const Vertex &a, const Vertex &b) {
  return a.position == b.position &&
  a.normal == b.normal &&
  a.textureCoordinate == b.textureCoordinate;
}

Vertex operator+(const Vertex &a, const glm::vec3 &b) {
  return {a.position + b, a.normal, a.textureCoordinate};
}
