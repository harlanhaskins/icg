#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

/// Represents a position and viewing direction for a camera.
struct Camera {
  /// The position of the camera in scene coordinates.
  glm::vec3 eye;

  /// The position the camera is facing in scene coordinates.
  glm::vec3 lookAt;

  /// A 3D direction vector representing 'up' in the coordinate system.
  glm::vec3 up;

  /// Gets the corresponding view transform to translate model coordinates to
  /// view coordinates viewed from this camera.
  glm::mat4 getViewTransform() {
    return glm::lookAt(eye, lookAt, up);
  }
};

#endif /* _CAMERA_H_ */
