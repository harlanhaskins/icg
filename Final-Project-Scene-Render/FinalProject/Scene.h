#ifndef _SCENE_H_
#define _SCENE_H_

#include <vector>

#include "Camera.h"
#include "Light.h"
#include "Model.h"
#include "Projection.h"

/// Represents a modelable scene, with a set of models, a single camera, and
/// a single light scene.
struct Scene {
  /// The models present in this scene.
  std::vector<std::shared_ptr<Model>> models;

  /// The single camera for this scene.
  Camera camera;

  /// The single point light source in this scene.
  Light light;

  /// The projection of this scene.
  Projection projection;

private:
  // Private constructors.

  Scene() {}
  Scene(Camera camera, Light light, Projection projection)
    : camera(camera), light(light), projection(projection) {}

public:
  friend class Model;

  /// Creates an empty scene.
  static std::shared_ptr<Scene> create() {
    return std::shared_ptr<Scene>(new Scene());
  }

  /// Creates a scene with the provided camera, light, and projection.
  static std::shared_ptr<Scene>
  create(Camera camera, Light light, Projection projection) {
    return std::shared_ptr<Scene>(new Scene(camera, light, projection));
  }

  /// Adds the provided model to the scene.
  void addModel(std::shared_ptr<Model> model) {
    model->commit();
    models.push_back(model);
  }

  /// Draws the content of the scene.
  void draw();
};

#endif /* _SCENE_H_ */
