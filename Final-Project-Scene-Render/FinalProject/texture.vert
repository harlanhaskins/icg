#version 120

// Texture mapping vertex shader
//
// Contributor: Harlan Haskins

// Vertex location (in model space)
attribute vec4 vPosition;

// Normal vector at vertex (in model space)
attribute vec3 vNormal;

// Texture coordinate for this vertex
attribute vec2 vTexCoord;

// Here is where you should add the variables you need in order
// order to perform the vertex shader portion of the shading
// and texture mapping computations

varying vec2 textureCoord;

void main() {

    // Here is where you should all all your code to perform the
    // vertex shader portion of your lighting and shading work
    //
    // remember that transformation of the surface normal should not
    // include translations; see
    // http://www.lighthouse3d.com/tutorials/glsl-tutorial/the-normal-matrix/
    // for a discussion.  normal transformation should be done using the
    // inverse transpose of the upper-left 3x3 submatrix of the modelView
    // matrix.

    textureCoord = vTexCoord;

    // Transform the vertex location into clip space
    gl_Position = projMat * viewMat  * modelMat * vPosition;
}
