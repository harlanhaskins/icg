#version 120

// Phong fragment shader
//
// Contributor:  Harlan Haskins

uniform vec3 cPosition;

// Variables provided by the vertex shader.

varying vec3 N;
varying vec4 V;

// "constants" about the scene.

uniform vec4 lightPos;
uniform vec4 ambientColor;
uniform vec4 diffuseColor;
uniform vec4 specularColor;
uniform vec4 lightColor;
uniform vec4 ambientLightColor;

// Material lighting coefficients (k_{a, d, s})

uniform float ambientCoeff;
uniform float diffuseCoeff;
uniform float specCoeff;
uniform float specExp;

void main() {
  // Compute ambient light color.
  vec4 ambient = ambientLightColor * ambientColor * ambientCoeff;

  // Compute diffuse light color.
  vec3 vPos = V.xyz / V.w;
  vec3 lightDir = normalize(lightPos.xyz - vPos);
  float diffuseMult = max(dot(lightDir, N), 0.0);
  vec4 diffuse = lightColor * diffuseColor * diffuseCoeff * diffuseMult;

  // Compute specular light color.
  vec3 reflectionDir = reflect(-lightDir, N);
  vec3 viewDir = normalize(cPosition.xyz - vPos);
  float specularMult = pow(max(dot(viewDir, reflectionDir), 0.0), specExp);
  vec4 specular = lightColor * specularColor * specCoeff * specularMult;

  gl_FragColor = ambient + diffuse + specular;
}
