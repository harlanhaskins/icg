#version 120

// Phong vertex shader
//
// Contributor:  Harlan Haskins

// Vertex location (in model space)
attribute vec4 vPosition;

// Normal vector at vertex (in model space)
attribute vec3 vNormal;

// Model transform (model space -> view space)
attribute mat4 modelTransform;

// Normal transform (transpose of inverse of model transform)
attribute mat3 normalTransform;

// View transform (view space -> scene space)
attribute mat4 viewTransform;

// Projection transform (scene space -> screen space)
attribute mat4 projectionTransform;

// add all necessary variables for communicating with
// the fragment shader here

varying vec3 N;
varying vec4 V;

mat4 inverse(mat4);

void main()
{

  // Here is where you should all all your code to perform the
  // vertex shader portion of your lighting and shading work
  //
  // remember that transformation of the surface normal should not
  // include translations; see
  // http://www.lighthouse3d.com/tutorials/glsl-tutorial/the-normal-matrix/
  // for a discussion.  normal transformation should be done using the
  // inverse transpose of the upper-left 3x3 submatrix of the modelView
  // matrix.

  N = normalize(normalTransform * vNormal);
  V = modelTransform * vPosition;

  // Transform the vertex location into clip space
  gl_Position = projectionTransform * viewTransform * modelTransform * vPosition;
}
