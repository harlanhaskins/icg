#ifndef _PROJECTION_H_
#define _PROJECTION_H_

/// A method of projecting the camera's viewport.
struct Projection {
  enum class Kind {
    /// Orthographic projection.
    Orthographic,

    /// Frustum projection.
    Frustum
  };
  /// The kind of projection.
  Kind kind;
  glm::mat4 projectionMatrix;
public:
  /// Creates a frustum projection with the provided aspect ratio and clipping
  /// planes.
  static Projection
  frustum(
    float aspectRatio,
    float left, float right,
    float top, float bottom,
    float near, float far
  ) {
    return {
      Kind::Frustum,
      glm::frustum(left * aspectRatio, right * aspectRatio, top, bottom, near, far)
    };
  }

  /// Creates an orthographic projection with the provided aspect ratio and
  /// clipping planes.
  static Projection
  orthographic(
    float aspectRatio,
    float left, float right,
    float top, float bottom,
    float near, float far
  ) {
    return {
      Kind::Orthographic,
      glm::ortho(left * aspectRatio, right * aspectRatio, top, bottom, near, far)
    };
  }

  /// Gets a projection matrix from this projection.
  glm::mat4 getMatrix() { return projectionMatrix; }
};

#endif /* _PROJECTION_H_ */
