#include "Shader.h"
#include "Texture.h"
#include <iostream>

void Texture::bind(Shader &shader, const char *name) {
  auto loc = glGetUniformLocation(shader.getProgram(), name);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, handle);
  glUniform1i(loc, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

std::shared_ptr<Texture> Texture::load(const char *filename) {
  auto tex = SOIL_load_OGL_texture(
    filename,
    SOIL_LOAD_AUTO,
    SOIL_CREATE_NEW_ID,
    SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y
  );

  if (tex == 0) {
    std::cerr << "Failed loading texture '" << filename << "': "
              << SOIL_last_result() << std::endl;
    exit(-1);
  }

  return std::shared_ptr<Texture>(new Texture(tex));
}
