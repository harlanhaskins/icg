#ifndef _VERTEX_H_
#define _VERTEX_H_

#include <glm/glm.hpp>
#include <glm/gtx/hash.hpp>

#include "Utils.h"

/// Represents a vertex, along with its normal and texture coordinate.
struct Vertex {
  glm::vec3 position;
  glm::vec3 normal;
  glm::vec2 textureCoordinate;

  Vertex(glm::vec3 position): position(position) {}
  Vertex(glm::vec3 position, glm::vec3 normal, glm::vec2 textureCoordinate)
    : position(position), normal(normal), textureCoordinate(textureCoordinate) {
  }
};

bool operator==(const Vertex &a, const Vertex &b);
Vertex operator+(const Vertex &a, const glm::vec3 &b);

/// Represents 3 vertices in a counter-clockwise triangle.
struct Triangle {
  Vertex vertices[3];

  void computeNormals() {
    auto normal = glm::normalize(
      glm::cross(
        vertices[2].position - vertices[0].position,
        vertices[1].position - vertices[0].position
      )
    );
    vertices[0].normal = normal;
    vertices[1].normal = normal;
    vertices[2].normal = normal;
  }
};

namespace std {
  template <>
  struct hash<Vertex> {
    size_t operator()(const Vertex& k) const {
      size_t result = 0;
      std::hash_combine(result, k.position);
      std::hash_combine(result, k.normal);
      std::hash_combine(result, k.textureCoordinate);
      return result;
    }
  };
}


#endif /* _VERTEX_H_ */
