#ifndef _MATERIAL_H_
#define _MATERIAL_H_

#include <glm/glm.hpp>
#include <memory>
#include "Shader.h"
#include "Texture.h"

/// Represents a material that can be phong shaded, textured, or both.
class Material {
  /// The shader which will render the material.
  std::shared_ptr<Shader> shader;

  /// Sets the properties of this material in the chader.
  void setProperties();

  /// Private default constructor for Materials.
  Material() {}
public:
  /// Model needs to be able to call `setProperties`.
  friend class Model;

  /// The texture of this model. If this is NULL, then the model will be simply
  /// phong-shaded. Otherwise, the material color will be blended with the
  /// texture color.
  std::shared_ptr<Texture> texture = nullptr;

  /// The base color of the material. Used for ambient, diffuse, and specular
  /// lighting.
  glm::vec3 color = glm::vec3(1);

  /// How many times to repeat the texture along the X and Y axes of this model.
  glm::vec2 textureScaling = glm::vec2(1, 1);

  /// Coefficient for ambient lighting.
  float ambient = 0.5;

  /// Coefficient for diffuse lighting.
  float diffuse = 0.7;

  /// Coefficient for specular lighting.
  float specular = 1.0;

  /// Exponent for specular highlights.
  float specularExponent = 64.0;


  /// Creates a new material rendered by a given shader.
  ///
  /// @param shader The shader that should render this material.
  /// @param texture The optional texture to texture this material.
  /// @return A new material rendered by the provided shader and texture.
  static std::shared_ptr<Material> create(
    std::shared_ptr<Shader> shader,
    std::shared_ptr<Texture> texture = nullptr
  ) {
    auto mat = std::shared_ptr<Material>(new Material());
    mat->shader = shader;
    mat->texture = texture;
    return mat;
  }

  /// Creates a copy of this material with a different material color.
  std::shared_ptr<Material> withColor(glm::vec3 color);

  /// Creates a copy of this material with a different texture.
  std::shared_ptr<Material> withTexture(std::shared_ptr<Texture> texture);
};

#endif /* _MATERIAL_H_ */
