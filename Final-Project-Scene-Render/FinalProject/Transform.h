#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include <glm/glm.hpp>

/// Represents a set of scale, rotation, and translation transforms.
struct Transform {
  glm::vec3 scalar = glm::vec3(1);
  glm::vec3 translation = glm::vec3(0);
  glm::vec3 rotation = glm::vec3(0);

  Transform() {}

  /// Gets a model matrix from this transform.
  glm::mat4 getMatrix();

  void setTranslation(glm::vec3 translation);
  void setScale(glm::vec3 scalar);
  void setRotation(glm::vec3 rotation);
  void setRotationDegrees(glm::vec3 rotation);
  void translateBy(glm::vec3 translation);
  void scaleBy(glm::vec3 scale);
  void rotateBy(glm::vec3 rot);
  void rotateByDegrees(glm::vec3 rotation);
};

#endif /* _TRANSFORM_H_ */
