#ifndef _MODEL_H_
#define _MODEL_H_

#include "Mesh.h"
#include "Material.h"
#include "Shader.h"
#include "Texture.h"
#include "Transform.h"

class Scene;

/// Represents a modeled object in the scene.
class Model {
  /// The mesh this model will eventually draw.
  std::shared_ptr<Mesh> mesh;

  /// The phong-shaded material properties of this model.
  /// If the `texture` field is populated, this value is ignored.
  std::shared_ptr<Material> material;

  /// The transform to apply to the object before rendering.
  Transform transform = {};

  /// Add this model's bits to the GPU.
  void commit();

  /// Private constructor for Model.
  Model(
    std::shared_ptr<Mesh> mesh,
    std::shared_ptr<Material> material,
    Transform transform
  ) : mesh(mesh), material(material), transform(transform) {}
public:
  friend class Scene;

  /// Creates a Model with the provided mesh and material, optionally passing
  /// in a known transform.
  static std::shared_ptr<Model>
  create(
    std::shared_ptr<Mesh> mesh,
    std::shared_ptr<Material> material,
    Transform transform = {}
  ) {
    return std::shared_ptr<Model>(
      new Model(mesh, material, transform)
    );
  }

  /// The model matrix, provided by this model's transform.
  glm::mat4 getTransformMatrix() {
    return transform.getMatrix();
  }

  /// Clears the existing transform on this model.
  void clearTransform() {
    transform = {};
  }

  void setTranslation(glm::vec3 translation);
  void setScale(glm::vec3 scalar);
  void setRotation(glm::vec3 rotation);
  void setRotationDegrees(glm::vec3 rotation);
  void translateBy(glm::vec3 translation);
  void scaleBy(glm::vec3 scale);
  void rotateBy(glm::vec3 rotation);
  void rotateByDegrees(glm::vec3 rotation);
  void draw(Scene &scene);
};

#endif /* _MODEL_H_ */
