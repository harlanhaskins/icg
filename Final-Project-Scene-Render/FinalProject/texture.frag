#version 120

// Texture mapping vertex shader
//
// Contributor: Harlan Haskins

// The smile and frown textures loaded from disk.
uniform sampler2D smileTexture;
uniform sampler2D frownTexture;

// The texture coordinate supplied by the vertex shader.
varying vec2 textureCoord;

void main() {
  // Determine if the fragment is facing the front. If it is, use the smile
  // texture. Otherwise, frown.
  gl_FragColor = texture2D(gl_FrontFacing ? smileTexture : frownTexture, textureCoord);
}
