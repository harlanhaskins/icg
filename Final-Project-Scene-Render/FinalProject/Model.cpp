#include "Model.h"
#include "Scene.h"
#include "Transform.h"
#include <iostream>

using namespace glm;

void Model::commit() {
  mesh->finalize(*material->shader);
}

void Model::translateBy(glm::vec3 translation) {
  transform.translateBy(translation);
}

void Model::scaleBy(glm::vec3 scale) {
  transform.scaleBy(scale);
}

void Model::rotateBy(glm::vec3 rotation) {
  transform.rotateBy(rotation);
}

void Model::rotateByDegrees(glm::vec3 rotation) {
  transform.rotateByDegrees(rotation);
}

void Model::setTranslation(glm::vec3 translation) {
  transform.setTranslation(translation);
}

void Model::setScale(glm::vec3 scalar) {
  transform.setScale(scalar);
}

void Model::setRotation(glm::vec3 rotation) {
  transform.setRotation(rotation);
}

void Model::setRotationDegrees(glm::vec3 rotation) {
  transform.setRotationDegrees(rotation);
}

void Model::draw(Scene &scene) {
  auto shader = material->shader;
  glUseProgram(shader->getProgram());

  auto modelTransform = getTransformMatrix();
  auto mvp = scene.projection.getMatrix() *
             scene.camera.getViewTransform() *
             modelTransform;
  shader->setMat4("modelTransform", modelTransform);
  shader->setMat4("mvpTransform", mvp);

  auto normalMatrix = transpose(inverse(mat3(modelTransform)));
  shader->setMat3("normalTransform", normalMatrix);

  shader->setVec3("cameraPosition", scene.camera.eye);
  shader->setVec3("lightPosition", scene.light.position);
  shader->setVec3("lightColor", scene.light.color);

  material->setProperties();

  mesh->draw(*shader);
}
