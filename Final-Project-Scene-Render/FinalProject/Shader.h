#ifndef _SHADER_H_
#define _SHADER_H_

#include <memory>
#include <string>
#include <experimental/optional>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

/// Represents both a vertex and fragment shader, compiled together.
class Shader {
  /// A handle to the OpenGL program represented by this shader.
  GLuint program;

  /// A private constructor for shaders.
  Shader(GLuint program): program(program) {}
public:
  /// One of the possible shader errors during shader compilation.
  enum class Error {
    None, VertexLoad, FragmentLoad, VertexCompile,
    FragmentCompile, Link
  };

  /// Creates a Shader, attempting to compile the provided vertex and fragment
  /// shaders.
  static std::shared_ptr<Shader> create(
    const char *vertexPath,
    const char *fragmentPath,
    Error &out
  );

  GLuint getProgram() {
    return program;
  }

  /// Gets the location of the provided attribute name.
  GLuint attribLocation(const char *name) {
    return glGetAttribLocation(program, name);
  }

  void setBool(const char *name, bool b) {
    auto loc = glGetUniformLocation(program, name);
    glUniform1i(loc, (int)b);
  }

  void setFloat(const char *name, float f) {
    auto loc = glGetUniformLocation(program, name);
    glUniform1f(loc, f);
  }

  void setVec3(const char *name, glm::vec3 v) {
    auto loc = glGetUniformLocation(program, name);
    glUniform3fv(loc, 1, glm::value_ptr(v));
  }

  void setVec2(const char *name, glm::vec2 v) {
    auto loc = glGetUniformLocation(program, name);
    glUniform2fv(loc, 1, glm::value_ptr(v));
  }

  void setMat3(const char *name, glm::mat3 m, bool transpose = false) {
    auto loc = glGetUniformLocation(program, name);
    glUniformMatrix3fv(loc, 1, transpose, glm::value_ptr(m));
  }

  void setMat4(const char *name, glm::mat4 m, bool transpose = false) {
    auto loc = glGetUniformLocation(program, name);
    glUniformMatrix4fv(loc, 1, transpose, glm::value_ptr(m));
  }
};

std::string shaderErrorString(Shader::Error error);

#endif /* _SHADER_H_ */
