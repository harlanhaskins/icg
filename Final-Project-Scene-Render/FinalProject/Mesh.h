#ifndef _MESH_H
#define _MESH_H

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <vector>
#include <map>
#include <unordered_map>
#include <memory>
#include "Utils.h"
#include "Vertex.h"

class Shader;

class Mesh {
  std::vector<glm::vec3> vertexPositions;
  std::vector<glm::vec3> vertexNormals;
  std::vector<glm::vec2> vertexTextureCoordinates;
  std::vector<GLuint> elements;
  std::unordered_map<Vertex, GLuint> indexTable;

  GLuint vertexBuffer = 0;
  GLuint elementBuffer = 0;

  /// Creates OpenGL buffers for this mesh.
  void finalize(Shader &shader);

  /// Draws the OpenGL elements of this mesh.
  void draw(Shader &shader);

  Mesh() {}
public:
  friend class Model;
  
  /// Disable copy constructor for Mesh.
  Mesh(Mesh &m) = delete;

  static std::shared_ptr<Mesh> create();

  /// Generates a mesh for a unit sphere with the provided icosahedral
  /// subdivisions.
  static std::shared_ptr<Mesh> sphere(int subdivisions);

  /// Generates a mesh for a unit cube.
  static std::shared_ptr<Mesh> cube();

  /// Generates a mesh for a unit cylinder.
  static std::shared_ptr<Mesh>
  cylinder(int radialDivisions, int heightDivisions);

  /// Generates a mesh for a cone.
  static std::shared_ptr<Mesh>
  cone(int radialDivisions, int heightDivisions);

  /// Adds a vertex to this mesh.
  void addVertex(Vertex vert);

  /// Adds three vertices to this mesh, optionally setting their normals to the
  /// triangle's face normal.
  void addTriangle(Triangle triangle, bool inferNormals = false);

  /// Adds four vertices to this mesh as two triangles in a quad, optionally
  /// setting their normals to the quad's face normal.
  void addQuad(Vertex lr, Vertex ur, Vertex ul, Vertex ll, bool inferNormals = false);

  /// Adds four vertices to this mesh as two triangles in a quad, optionally
  /// setting their normals to the quad's face normal.
  void addQuad(glm::vec3 lr, glm::vec3 ur, glm::vec3 ul, glm::vec3 ll,
               bool inferNormals = false);
};

#endif /* _MESH_H */
