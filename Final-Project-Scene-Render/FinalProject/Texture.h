#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>

class Shader;

/// Represents a loaded OpenGL texture.
class Texture {
  /// The OpenGL texture handle.
  GLuint handle;

  /// Binds this texture to the provided variable in the provided shader.
  void bind(Shader &shader, const char *name);

  /// Private constructors.
  Texture() {}
  Texture(GLuint handle): handle(handle) {}
public:
  friend class Material;

  /// Loads an image into a Texture object.
  static std::shared_ptr<Texture> load(const char *path);
};

#endif /* _TEXTURE_H_ */
