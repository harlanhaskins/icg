#include "Scene.h"

void Scene::draw() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  for (auto model : models) {
    model->draw(*this);
  }
}
