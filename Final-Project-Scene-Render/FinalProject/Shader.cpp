#include "Shader.h"
#include <iostream>

std::string shaderErrorString(Shader::Error error) {
  switch (error) {
    case Shader::Error::None: return "No error";
    case Shader::Error::VertexLoad: return "Error loading vertex shader";
    case Shader::Error::FragmentLoad: return "Error loading fragment shader";
    case Shader::Error::VertexCompile: return "Error compiling vertex shader";
    case Shader::Error::FragmentCompile: return "Error compiling fragment shader";
    case Shader::Error::Link: return "Error linking shader";
  }
}

///
// readTextFile(name)
//
// Read the text file given as 'name'.
//
// Returns the contents of the file in a dynamically-allocated
// string buffer, or NULL if an error occurs.
///
GLchar *readTextFile( const char *name ) {
  GLchar *content = NULL;
  long count = 0;

  if (name == NULL) {
    fprintf( stderr, "error:  no file name specified\n" );
    return NULL;
  }

  // Attempt to open the file
  auto fp = fopen( name, "r" );
  if (fp == NULL) {
    perror( name );
    return NULL;
  }

  // Determine its length
  fseek( fp, 0, SEEK_END );
  count = ftell( fp );
  rewind( fp );

  if (count > 0) {
    // Allocate the string buffer
    content = new GLchar[ sizeof(GLchar) * (count+1) ];
    // Read the file into the buffer
    count = fread(content, sizeof(GLchar), count, fp);
    // Add a NUL terminator
    content[count] = '\0';
  }

  fclose(fp);

  return content;
}

enum class InfoLogKind {
  Program, Shader
};

static void printInfoLog(GLuint item, InfoLogKind kind) {
  char *log;
  GLint length;
  switch (kind) {
  case InfoLogKind::Program:
    glGetProgramiv(item, GL_INFO_LOG_LENGTH, &length);
  case InfoLogKind::Shader:
    glGetShaderiv(item, GL_INFO_LOG_LENGTH, &length);
  }
  log = new char[length];
  switch (kind) {
  case InfoLogKind::Program:
    glGetProgramInfoLog(item, length, NULL, log);
  case InfoLogKind::Shader:
    glGetShaderInfoLog(item, length, NULL, log);
  }
  if (log[0] != '\0') {
    std::cout << "Program log:\n" << log << "\n";
  }
}

std::shared_ptr<Shader> Shader::create(
  const char *vertexPath,
  const char *fragmentPath,
  Shader::Error &out
) {
  out = Shader::Error::None;

  auto vertexFile = readTextFile(vertexPath);
  if (!vertexFile) {
    out = Shader::Error::VertexLoad;
    return nullptr;
  }

  auto fragmentFile = readTextFile(fragmentPath);
  if (!fragmentFile) {
    out = Shader::Error::FragmentLoad;
    return nullptr;
  }

  GLint flag;

  auto vs = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vs, 1, &vertexFile, NULL);
  glCompileShader(vs);
  glGetShaderiv(vs, GL_COMPILE_STATUS, &flag);
  printInfoLog(vs, InfoLogKind::Shader);
  free(vertexFile);
  if (flag == GL_FALSE) {
    out = Shader::Error::VertexCompile;
    return nullptr;
  }

  auto fs = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fs, 1, &fragmentFile, NULL);
  glCompileShader(fs);
  glGetShaderiv(fs, GL_COMPILE_STATUS, &flag);
  printInfoLog(fs, InfoLogKind::Shader);
  free(fragmentFile);
  if (flag == GL_FALSE) {
    out = Shader::Error::FragmentCompile;
    return nullptr;
  }

  GLuint program = glCreateProgram();
  glAttachShader(program, vs);
  glAttachShader(program, fs);
  printInfoLog(program, InfoLogKind::Program);

  glLinkProgram(program);
  glGetShaderiv(fs, GL_LINK_STATUS, &flag);
  printInfoLog(program, InfoLogKind::Program);
  if (flag == GL_FALSE) {
    out = Shader::Error::Link;
    return nullptr;
  }

  return std::shared_ptr<Shader>(new Shader(program));
}
