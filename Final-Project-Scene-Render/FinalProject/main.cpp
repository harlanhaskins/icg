#include <cstdlib>
#include <iostream>
#include <GLFW/glfw3.h>
#include "Scene.h"

using namespace std;

void glfwError(int code, const char *desc) {
  cerr << "GLFW error " << code << ": " << desc << endl;
  exit(2);
}

int main(int argc, char **argv) {
  glfwSetErrorCallback(glfwError);

  if (!glfwInit()) {
    cerr << "Can't initialize GLFW!" << endl;
    exit(1);
  }

  const float width = 1440.0;
  const float height = 720.0;
  GLFWwindow *window = glfwCreateWindow(width, height, "Final Project", NULL, NULL);

  if (!window) {
    cerr << "GLFW window create failed!" << endl;
    glfwTerminate();
    exit(1);
  }

  glfwMakeContextCurrent(window);

  Shader::Error err;
  auto shader = Shader::create("phong.vert", "phong.frag", err);
  if (err != Shader::Error::None) {
    cerr << shaderErrorString(err) << endl;
    return -1;
  }

  glEnable(GL_DEPTH_TEST);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glClearDepth(1.0);


  auto camera = Camera {
    {-0.05, 0.2, 2.0}, // Position
    {0.0, 3.0, -10.0}, // LookAt
    {0.0, 1.0,  0.0}  // Up
  };

  auto light = Light {
    glm::vec3(1.0, 1.0, 0.8), // Color
    glm::vec3(0.25, 0.51, -0.96)
  };

  auto scene = Scene::create(
    camera, light,
    Projection::frustum(
      width / height,
      -1, 1, -1, 1, 0.9, 100.0
    )
  );

  // Create model for Luxo ball

  auto ballTexture = Texture::load("ball.png");

  auto ballMat = Material::create(shader, ballTexture);
  ballMat->specular = 3.0;
  ballMat->specularExponent = 10.0;
  auto whiteMaterial = ballMat->withTexture(nullptr);

  auto ball = Model::create(Mesh::sphere(7), ballMat);
  ball->scaleBy(glm::vec3(0.25));
  ball->rotateByDegrees({-75, 35, 0});
  ball->translateBy({0.65, -0.45, 0.25});
  scene->addModel(ball);

  // Create model for base of lamp

  auto lightBase = Model::create(Mesh::cylinder(32, 32), whiteMaterial);
  lightBase->scaleBy({1, 0.1, 1});
  lightBase->translateBy({-0.75, -0.6, 0});
  scene->addModel(lightBase);

  // Create models for support beams of lamp.

  auto supportScale = glm::vec3(0.05, 1.5, 0.05);

  auto lightSupport1 = Model::create(Mesh::cube(), whiteMaterial);
  lightSupport1->setScale(supportScale);
  lightSupport1->translateBy({-0.7, 0, 0.06});
  scene->addModel(lightSupport1);

  auto lightSupport2 = Model::create(Mesh::cube(), whiteMaterial);
  lightSupport2->setScale(supportScale);
  lightSupport2->translateBy({-0.65, 0, -0.04});
  scene->addModel(lightSupport2);

  auto lightSupport3 = Model::create(Mesh::cube(), whiteMaterial);
  lightSupport3->setScale(supportScale);
  lightSupport3->translateBy({-0.9, 0, -0.06});
  scene->addModel(lightSupport3);

  supportScale.y /= 2;

  auto lightSupport4 = Model::create(Mesh::cube(), whiteMaterial);
  lightSupport4->setScale(supportScale);
  lightSupport4->rotateByDegrees({0, -15, -45});
  lightSupport4->translateBy({-0.45, 1.0, 0.04});
  scene->addModel(lightSupport4);

  supportScale.y += 0.2;

  auto lightSupport5 = Model::create(Mesh::cube(), whiteMaterial);
  lightSupport5->setScale(supportScale);
  lightSupport5->rotateByDegrees({0, -15, -45});
  lightSupport5->translateBy({-0.65, 1.1, -0.01});
  scene->addModel(lightSupport5);

  auto lightSupportConnector = Model::create(Mesh::cube(), whiteMaterial);
  lightSupportConnector->setScale({0.35, 0.05, 0.25});
  lightSupportConnector->translateBy({-0.75, 0.75, 0});
  scene->addModel(lightSupportConnector);

  // Create models for the lampshade and bulb holder.

  auto bulbHolder = Model::create(Mesh::cylinder(30, 1), whiteMaterial);
  bulbHolder->setScale(glm::vec3(0.4));
  bulbHolder->translateBy({-0.25, 1.4, 0.0});
  bulbHolder->rotateByDegrees({0, 0, 25});
  scene->addModel(bulbHolder);

  auto lampshade = Model::create(Mesh::cone(30, 1), whiteMaterial);
  lampshade->rotateByDegrees({5, 0, 25});
  lampshade->translateBy({-0.16, 1.2, 0});
  scene->addModel(lampshade);

  // Create model for the floor

  auto woodTexture = Texture::load("wood.jpg");
  auto woodMat = whiteMaterial->withTexture(woodTexture);
  woodMat->textureScaling = {10, 5};
  auto floor = Model::create(Mesh::cube(), woodMat);
  floor->scaleBy(glm::vec3(30, 6, 0));
  floor->rotateBy({-M_PI/2, 0, 0});
  floor->translateBy({0, -0.65, 0});
  scene->addModel(floor);

  float lastFrame = glfwGetTime();
  while (!glfwWindowShouldClose(window)) {
    scene->draw();
    lastFrame = glfwGetTime();
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  glfwDestroyWindow(window);
  glfwTerminate();

  return 0;
}
